﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller : MonoBehaviour
{

    public GameObject PanelInfo;
    public Animation Tiger;
    public string walkanim;
    public string AttackAnim;
   
    private bool show = false;

    public void ShowHideInfo()
    {
        if (!show)
        {
            PanelInfo.SetActive(true);
            show = true;
        }
        else
        {
            PanelInfo.SetActive(false);
            show = false;
        }
    }

    public void PlayWalkAnim()
    {
        Tiger.Play(walkanim);
    }

   

    public void PlayAttackAnim()
    {
        Tiger.Play(AttackAnim);
    }
}